import { Component, OnInit } from '@angular/core';
import {Employee} from './employee';
import {EmpDataService} from './emp-data.service';
import {Router, ActivatedRoute} from '@angular/router';
import {Title} from '@angular/platform-browser';
@Component({
  selector: 'app-delete-employee',
  template: 'Deleting..',
})
export class DeleteEmployeeComponent implements OnInit {
  titleText="Delete Employee";
  id:number;
  employee: Employee;
  constructor(private empSer:EmpDataService, private router:Router, private route:ActivatedRoute, private title: Title) { }

  ngOnInit() {
    this.title.setTitle(this.titleText);
    this.route.params.subscribe(paramId=> {
      this.id=paramId.id
    })
    this.deleteEmployee(this.id);
  }
  deleteEmployee(id): void {
    this.empSer.deleteEmployee(this.id).subscribe(employee=>{
      this.router.navigate(["employees-list"])
    });
    
  }
}