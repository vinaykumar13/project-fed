import { Component, OnInit } from '@angular/core';
import { Employee } from './employee';
import { EmpDataService } from './emp-data.service';
import { Title } from '@angular/platform-browser';
@Component({
  selector: 'app-employees-list',
  templateUrl: './employees-list.component.html',
})
export class EmployeesListComponent implements OnInit {

  constructor(private emp_ser: EmpDataService, private title: Title) { }
  emp: Employee[];
  titleText="Employee List";
  ngOnInit() {
    this.title.setTitle(this.titleText);
    this.getEmployees();
  }
  getEmployees(): void {
    this.emp_ser.getEmployees().subscribe(employees=>{this.emp=employees});
  }
}