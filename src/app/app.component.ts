import { Component,OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Employee } from './employee';
import { EmpDataService } from './emp-data.service';
@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.css' ]
})
export class AppComponent implements OnInit  {
  titleText="Employee Management System";
  employees: Employee[]=[];
  constructor(private empSer: EmpDataService, private title:Title) {}
  ngOnInit() {
    this.title.setTitle(this.titleText);
    this.getEmployees();
  }
  public getEmployees() {
    this.empSer.getEmployees().subscribe(employees => {
      this.employees = employees;
    });
  }
}
