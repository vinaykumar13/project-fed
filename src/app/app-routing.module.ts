import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'
import { EmployeesListComponent } from './employees-list.component';
import { AddEmployeeComponent } from './add-employee.component';
import {DeleteEmployeeComponent} from './delete-employee.component';
import { EditEmployeeComponent} from './edit-employee.component';
import { EmployeeDetailsComponent} from './employee-details.component';
import { ErrorPathComponent } from './error-path.component';
const routes: Routes = [
  {
    path: 'employees-list', 
    component: EmployeesListComponent
  },
  {
    path: 'add-employee', 
    component: AddEmployeeComponent
  },
  {
    path: 'delete/:id', 
    component: DeleteEmployeeComponent
  },
  {
    path: 'edit/:id', 
    component: EditEmployeeComponent
  },
  {
    path: 'details/:id', 
    component: EmployeeDetailsComponent
  },
  

];
@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  declarations: [],
  exports:[RouterModule]
})
export class AppRoutingModule { }